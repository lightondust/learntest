var windowLoaded = false;
var stockItemSelected = -1;
var editMode = false;
var g_result;
var currentDate;

var URLS = {
	"serviceWorkerScope":"/stock/",
	"serviceWorker":"/stock/sw.js",
	"getCurrentDate":"/stock/currentDate",
	"getMyStockList" : "/stock/stockList/",
	"getFinancialData" : "/stock/financialData/",
	"addStock":"/stock/edit/add/",
	"deleteStock":"/stock/edit/delete/"
}

getMyStockList()

if ('serviceWorker' in navigator) {
	  navigator.serviceWorker.register(URLS.serviceWorker,{scope: URLS.serviceWorkerScope}).then(function(registration) {
	    // 登録成功
	    console.log('ServiceWorker registration successful with scope: ', registration.scope);
	  }).catch(function(err) {
	    // 登録失敗 :(
	    console.log('ServiceWorker registration failed: ', err);
	  });
	}

window.onload = function() {
	console.log("window loaded");
	windowLoaded = true;

	if (document.URL.endsWith("edit")) {
		editMode = true;
	}
	
	makeRequest(params={
			"url" : URLS.getCurrentDate,
			"func200" : 
				function(res){
					currentDate = res.date;
				}
	});
}

function addStock(code) {
	console.log("call addStock:"+code);
	var params={};
	params.url=URLS.addStock + code;
	params.func200 = function(res){
		console.log(res);
	};
	makeRequest(params);
}

function deleteStock(code) {
	console.log("call deleteStock:"+code);
	var params={};
	params.url=URLS.deleteStock + code;
	params.func200 = function(res){
		console.log(res);
	};
	makeRequest(params);
}

function getMyStockList() {
	/*
	 * ページ読み込み時に登録銘柄を取得し、 onload後に表示させる関数displayMyStockListを呼び出す
	 */

	console.log("call getMyStockList");
	var params = {};

	params.url = URLS.getMyStockList + "default";
	params.func200 = displayStockList;
	makeRequest(params);
}

function displayStockList(stockList) {
	console.log("call displayStockList");
	var elem = document.getElementById("stockList");
	var code;
	var name;
	for (i = 0; i < stockList.length; i++) {
		code = stockList[i]["code"];
		name = stockList[i]["name"]
		elem.innerHTML += "<div class=\"stockItem\" id =\"" + code + "\""
				+ "onclick='selectFinancialData(" + code + ")'>" + "<div>"
				+ name + "<br />" + "(" + code + ")" + "</div>" + "</div>";

	}
}

function searchFinancialData() {
	// 検索の時
	console.log("call searchFinancialData");

	var code = document.getElementById("codeInputForFinancialData").value;

	if (stockItemSelected > 100) {
		document.getElementById(stockItemSelected).childNodes[0].style.fontWeight = "";
	}
	stockItemSelected = 0;

	showFinancialData(code);
}

function selectFinancialData(code) {
	// 選択の時
	console.log("call selectFinancialData");

	var elem = document.getElementById(code);
	var elemTable = document.getElementById("financialData");

	// すでに表示された状態でクリックされたら、テーブルを閉じる、表示されていないなら、表示させる
	if (code === stockItemSelected) {
		// 開いている銘柄を再度クリックした場合
		document.getElementById(stockItemSelected).childNodes[0].style.fontWeight = "";
		elemTable.style.display = "none";
		stockItemSelected = -1;
		return;
	} else if (stockItemSelected < 1) {
		// すべての銘柄が閉じている場合
		// elemTable.style.display="inline-block";
		elem.childNodes[0].style.fontWeight = "bold";
		stockItemSelected = code;
	} else {
		// ほかの銘柄が開いているときに別の銘柄をクリックした場合
		// elemTable.style.display="inline-block";
		elem.childNodes[0].style.fontWeight = "bold";
		document.getElementById(stockItemSelected).childNodes[0].style.fontWeight = "";
		stockItemSelected = code;
	}

	showFinancialData(code);
}

/* 財務データを取得して、表示させる */
function showFinancialData(code) {
	console.log("call showFinancialData");
	
	var params = {};
	if(currentDate){
		params.url = URLS.getFinancialData + code + "/" + currentDate;		
	} else{
	params.url = URLS.getFinancialData + code;
	}
	params.func200 = displayFinancialData;

	makeRequest(params);
}


function displayFinancialData(financialData, toId) {
	console.log("call displayFinancialData");
	if(!toId){
		toId = stockItemSelected;
	}

	var elem = document.getElementById("financialData");
	var target = document.getElementById(toId);
	target.appendChild(elem);

	var item = [ "year", "sales", "operating_profit", "net_income" ]
	var str;

	var elemBody = document.getElementById("financialDataTableTbody");
	elemBody.innerHTML = "";
	for (data_i = 0; data_i < financialData.length; data_i++) {
		str = "";
		for (item_i = 0; item_i < item.length; item_i++) {
			str += "<td>" + financialData[data_i][item[item_i]] + "</td>"
		}
		str = "<tr>" + str + "</tr>";
		elemBody.innerHTML += str;
	}
	var elemTable = document.getElementById("financialData");
	elemTable.style.display = "inline-block";

	var caption = document.getElementById("financialDataTableCaption");
	var name = financialData[0]["name"];
	var code = financialData[0]["code"];
	if (stockItemSelected === 0) {
		caption.innerHTML = name + "(" + code + ") <br />";
		if (editMode) {
			caption.innerHTML += "<button onclick=\"addStock("+ code + ")\">追加</button><br />";
		}
	} else {
		document.getElementById("financialDataTableCaption").innerHTML = "";
		if (editMode) {
			caption.innerHTML += "<button onclick=\"deleteStock("+ code + ")\">削除</button><br />";
		}
	}
	if(financialData[0]["per"]){
		var temp = financialData[0];
		caption.innerHTML += "per:" + temp["per"] + ", 株価：" + temp["last"] + ", <br />(" + temp["date"] +")";
	}
}

function makeRequest(params) {
	// params.urlが必須
	// params.func200、params.method、params.funcNot200、params.funcErrorはオプション
	console.log("make Request:" + params.url);
	if (!params.method) {
		params.method = "GET";
	}
	var result;

	var xhr = new XMLHttpRequest();
	xhr.open(params.method, params.url, true);
	xhr.onload = function(e) {
		console.log("get response");
		if (xhr.readyState === 4) {
			if (xhr.status === 200) {
				console.log(xhr.responseText);
				result = JSON.parse(xhr.response);
				if (params.func200) {
					if (windowLoaded) {
						params.func200(result);
					} else {
						window.onload = function() {
							console.log("wait for window load: getMyStockList");
							params.func200(result);
						}
					}
				} else{
					return g_result=result;
				}
			} else {
				if (params.funcNot200) {
					params.funcNot200();
				} else {
					console.error(xhr.statusText);
				}
			}
		}
	};
	xhr.onerror = function(e) {
		if (params.funcError) {
			params.funcError();
		} else {
			console.error(xhr.statusText);
		}
	};
	xhr.send(null);
}
