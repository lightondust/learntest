var CACHE_NAME = 'stock_171218v7';
var urlsToCache = [ 
	'/stock/top', 
	'/stock/img/logo.png', 
	'/stock/js/stock.js' ];

self.addEventListener('install', function(event) {
	// インストール処理
	event.waitUntil(caches.open(CACHE_NAME).then(function(cache) {
		console.log('Opened cache');
		return cache.addAll(urlsToCache);
	}));
});

self.addEventListener('activate', function(event) {
	  event.waitUntil(
	    caches.keys().then(function(cacheNames) {
	      return Promise.all(
	        cacheNames.filter(function(cacheName) {
	          // Return true if you want to remove this cache,
	          // but remember that caches are shared across
	          // the whole origin
	        	if(cacheName !== CACHE_NAME){return true;}
	        }).map(function(cacheName) {
	          return caches.delete(cacheName);
	        })
	      );
	    })
	  );
	});