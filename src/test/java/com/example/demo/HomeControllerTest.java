package com.example.demo;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.controller.ContentController;
import com.example.demo.entity.PerEntity;
import com.example.demo.mapper.PerMapper;

public class HomeControllerTest {
	@Autowired
	PerMapper perMapper;

	@Test
	@Ignore
	public void testPer() {
		ContentController homeController = new ContentController();
		ArrayList<PerEntity> res = homeController.perListByDate("20171201");
		assertEquals(17.9, res.get(0).getPer(), 0.2);
	}
}
