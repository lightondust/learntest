package com.example.demo;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entity.FinancialDataEntity;
import com.example.demo.mapper.FinancialDataMapper;


@RunWith(SpringRunner.class)
@MybatisTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class FinancialDataMapperTest {
	@Autowired
	FinancialDataMapper financialDataMapper;

	@Test
	public void test() {
		ArrayList<FinancialDataEntity> res = financialDataMapper.financialDataByCode(9997);
		assertThat(res.get(0).getYear(), is("2017"));
		assertThat(String.valueOf(res.get(0).getSales()), is("140000.0"));
	}

}
