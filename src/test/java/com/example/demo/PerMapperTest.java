package com.example.demo;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entity.PerEntity;
import com.example.demo.mapper.PerMapper;

@RunWith(SpringRunner.class)
@MybatisTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PerMapperTest {
	@Autowired
	PerMapper perMapper;

	@Ignore
	@Test
	public void test() {
		ArrayList<PerEntity> res = perMapper.perByDate("20171201");
		assertEquals(17.9, res.get(0).getPer(), 0.2);
		
	}

}
