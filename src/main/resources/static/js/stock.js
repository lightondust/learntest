//TODO: sw.js とonesignalの共存
var USINGSERVICEWORKER = false;

//TODO: windowloadを適切に設定出来ない問題
var windowLoaded = false;
var stockItemSelected = -1;
var g_result;
var currentDate;
var dateList;
if(typeof debugMode === "undefined"){
	var debugMode=false;
}
if(typeof editMode === "undefined"){
	var editMode=false;
}

var userName = "default";
var section = "default";
var URLS = {
	serviceWorkerScope: "/stock/",
	serviceWorker: "/stock/sw.js",
	drawLib: "/stock/js/flotr2.min.js",
	getCurrentDate: "/stock/currentDate",
	getDateList:"/stock/dateList",
	getPer:"/stock/per/",
	getPrice:"/stock/price/",
	addStock: "/stock/edit/add/",
	deleteStock: "/stock/edit/delete/",
	perSort: "/stock/fragment/perSort/",
	getMyStockList: "/stock/fragment/stockList/",
	getFinancialData: "/stock/fragment/financialData/",
}

/*表示内容取得*/
getMyStockList(preCache=USINGSERVICEWORKER);

var debugLog = function(message){
	if(debugMode){
		document.getElementById("debug").innerHTML += message + "<br />";
	}
}

/*Service Worker 初期設定*/
window.addEventListener("load",function(){
	if(USINGSERVICEWORKER){
		if ('serviceWorker' in navigator) {
			debugLog("ServiceWorker Available");
			navigator.serviceWorker.register(
				URLS.serviceWorker,
				{scope: URLS.serviceWorkerScope}
			).then(function(registration) {
				// 登録成功
				debugLog("ServiceWorker registrated");
				console.log('ServiceWorker registration successful with scope: ', registration.scope);
			}).catch(function(err) {
				// 登録失敗
				console.log('ServiceWorker registration failed: ', err);
				debugLog("ServiceWorker registration failed");
			});
		}else{
			debugLog("ServiceWorker is not available");
		}
	}else{
		debugLog("Not using ServiceWorker sw.js from stock.js");
		if ('serviceWorker' in navigator) {
			debugLog("ServiceWorker Available");
		}else{
			debugLog("ServiceWorker is not available");		
		}
	} 
	
});

window.addEventListener("load",function() {
	console.log("window onload");
	windowLoaded = true;
	getDateList();
	// IEの場合は何も表示しない
	(function(){
		var user = window.navigator.userAgent;
		if (user.match("MSIE") + user.match("Trident")){
			document.body.innerHTML = 
			"<p>This site is not for internet explorer. Google Chrome is recommanded.</p>" +
			"<p>本サイトはInternet explorerに対応しません. <a href=\"https://www.google.co.jp/chrome\">Google Chrome</a>が推奨されます.</p>";
		}
	})();
});

window.addEventListener("load",function(){
	console.log("window onload stock.js");
});
window.onload = function(){
	console.log("window onload stock.js 2nd");
}


/*銘柄の追加・削除*/
function addStock(code) {
	console.log("call addStock:"+code);
	if(!document.getElementById(code)){
		console.log("exe add");
		var params={};
		params.url=URLS.addStock + code;
		params.func200 = function(res){
			console.log(res);
			getMyStockList();
		};
		makeRequest(params);
	}
}

function deleteStock(code) {
	console.log("call deleteStock:"+code);
	var params={};
	params.url=URLS.deleteStock + code;
	params.func200 = function(res){
		console.log(res);
		getMyStockList();
	};
	makeRequest(params);
}


/*銘柄一覧取得*/
function getMyStockList(preCache) {
	/*
	 * ページ読み込み時に登録銘柄を取得し、 onload後に表示させる関数displayMyStockListを呼び出す
	 */

	console.log("call getMyStockList, preChach:" + preCache);
	var params = {};

	params.url = URLS.getMyStockList + userName + "/" + section;
	params.mode = "text";

	if(preCache){
		params.func200 = preCacheMyStockList;
	}else{
		params.func200 = function(resStockList){
			displayStockList(resStockList);
			getCurrentDate();
		};
	}
	makeRequest(params);
}

/*銘柄一覧の財務データをPre読み込み*/
function preCacheMyStockList(stockList){

	// 銘柄一覧を表示
	displayStockList(stockList);

	// 直近の日付け取得、コールバックでPreリクエスト
	getCurrentDate(getFinancialDatas);

	//財務データ取得
	var params = {};

	function getFinancialDatas(){
		var stockListElems = document.getElementsByClassName("stockItem");
		var code;
		for(i=0; i<stockListElems.length; i++){
			code = stockListElems[i].id;
			if(code>0){
				var paramsPre = {};
				paramsPre.url = URLS.getFinancialData + code + "/" + currentDate + "/normal";
				paramsPre.mode = "text";
				makeRequest(paramsPre);
			}
		}
	}
}

/*銘柄一覧を表示させる*/
function displayStockList(stockList) {
	console.log("call displayStockList");
	document.getElementById("stockList").outerHTML = stockList;
}

// 直近の日付け取得
function getCurrentDate(callback){
	var params={}
	params.url = URLS.getCurrentDate;
	if(callback){
		params.func200 = function(currentDateResponse){
			currentDate = currentDateResponse.date;
			callback();
		}
	} else {
		params.func200 = function(currentDateResponse){
			currentDate = currentDateResponse.date;
		}
	}
	makeRequest(params);	
}

// 日付一覧取得
function getDateList(){
	makeRequest(params={
		url:URLS.getDateList,
		func200:function(res){
			dateList = res.map(function(item){
				return item.date;
			});
		}
	});
}

// perソート表示
var perSort={
	switchPer : function(){
		if (!perSort.getElement().innerHTML){
			perSort.showPer();	
		}else {
			perSort.closePer();
		}
	},

	showPer : function(givenDate){
		var params = {};
		var selectDate;
		if(givenDate){
			selectDate = givenDate;
		} else{
			selectDate = currentDate;
		}
		params.url = URLS.perSort + selectDate;
		params.func200 = perSort.displayPer;
		params.mode="text";
		makeRequest(params);
	},

	displayPer : function (res){
		perSort.getElement().innerHTML = res;
	},

	closePer : function(){
		perSort.displayPer("");
	},

	getElement : function(){
		return document.getElementById("perSort");
	}
};

// スクリプト読込
function loadScript(url, callback) {
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = url;

	script.onload = function() {
		if(callback){
			callback();
		}
	};

	document.getElementsByTagName('head')[0].appendChild(script);
};

// グラフ:per
function drawPer(dateIndex){
	var selectDate;

	var elemNext = document.getElementById("perDistributionNext");
	var elemPrevious = document.getElementById("perDistributionPrevious");
	var elemRedo = document.getElementById("perDistributionRedo");

	var step = document.getElementById("perDistributionStep").value;

	// dateIndexが指定されていない場合は０と見なす
	if(dateIndex){
		elemNext.class = "clickable";
		var nextIndex = parseInt(dateIndex) - 1;
		elemNext.onclick = function(){
			drawPer(nextIndex, step);
		};
	} else{
		elemNext.class = "";
		elemNext.onclick = "";
		dateIndex = 0;
	}

	if(dateIndex < dateList.length){
		elemPrevious.class = "clickable";
		var previousIndex = parseInt(dateIndex) + 1;
		elemPrevious.onclick = function(){
			drawPer(previousIndex, step);
		};
	} else {
		elemPrevious.class = "";
		elemPrevious.onclick = "";		
	}

	elemRedo.onclick = function(){
		drawPer(dateIndex, step);
	};

	selectDate = dateList[dateIndex];

	var drawPerData = function(perList){		
		perData = densityFromPerList(perList, step);
		Flotr.draw(
			document.getElementById("perDistributionGraph"),
			[{ data: perData, lines: {show:true} }],
			{title:selectDate}
		);
	}

	makeRequest(params={
		url : URLS.getPer + selectDate,
		func200 : drawPerData
	});

}

// 描画するためのデータを整形する、一定領域内の銘柄数
function densityFromPerList(perList,step){
	var densityList=[];
	var j=0;
	var density=0;
	for(i = 0; i < perList.length; i++){
		if(perList[i].per < step*j){
			density++;
		}else{
			densityList[j]=[j*step,density/step];
			j++;
			density=0;
		}
	}
	return densityList;
}

// 描画するか、グラグを消すか、きり分け
function beginDrawingPer(){
	if(dateList){
	} else {
		getDateList();
	}

	var elemPerGraph = document.getElementById("perDistributionGraph");
	var elemPerDist = document.getElementById("perDistribution");
	if(elemPerGraph.innerHTML){
		//描画されたグラフを閉じる場合
		elemPerGraph.innerHTML = "";
		elemPerDist.style = "display:none;"
	} else{
		// グラフを描画する
		elemPerDist.style = "display:inline-block;";

		if(typeof Flotr !== "undefined"){
			drawPer();
		}else{
			loadScript(URLS.drawLib,drawPer);
		}
	}
}

// グラフ:price
function drawPrice(dateIndex){
	var selectDate;

	var elemNext = document.getElementById("priceDistributionNext");
	var elemPrevious = document.getElementById("priceDistributionPrevious");
	var elemRedo = document.getElementById("priceDistributionRedo");

	var step = document.getElementById("priceDistributionStep").value;

	// dateIndexが指定されていない場合は０と見なす
	if(dateIndex){
		elemNext.class = "clickable";
		var nextIndex = parseInt(dateIndex) - 1;
		elemNext.onclick = function(){
			drawPrice(nextIndex, step);
		};
	} else{
		elemNext.class = "";
		elemNext.onclick = "";
		dateIndex = 0;
	}

	if(dateIndex < dateList.length){
		elemPrevious.class = "clickable";
		var previousIndex = parseInt(dateIndex) + 1;
		elemPrevious.onclick = function(){
			drawPrice(previousIndex, step);
		};
	} else {
		elemPrevious.class = "";
		elemPrevious.onclick = "";		
	}

	elemRedo.onclick = function(){
		drawPrice(dateIndex, step);
	};

	selectDate = dateList[dateIndex];

	var drawPriceData = function(priceList){		
		priceData = densityFromPriceList(priceList, step);
		Flotr.draw(
			document.getElementById("priceDistributionGraph"),
			[{ data: priceData, lines: {show:true} }],
			{title:selectDate}
		);
	}

	makeRequest(params={
		url : URLS.getPrice + selectDate,
		func200 : drawPriceData
	});

}

// 描画するためのデータを整形する、一定領域内の銘柄数
function densityFromPriceList(priceList,step){
	var densityList=[];
	var j=0;
	var density=0;
	for(i = 0; i < priceList.length; i++){
		if(priceList[i].last < step*j){
			density++;
		}else{
			densityList[j]=[j*step,density/step];
			j++;
			density=0;
		}
	}
	return densityList;
}

// 描画するか、グラグを消すか、きり分け
function beginDrawingPrice(){
	if(dateList){
	} else {
		getDateList();
	}

	var elemPriceGraph = document.getElementById("priceDistributionGraph");
	var elemPriceDist = document.getElementById("priceDistribution");
	if(elemPriceGraph.innerHTML){
		//描画されたグラフを閉じる場合
		elemPriceGraph.innerHTML = "";
		elemPriceDist.style = "display:none;"
	} else{
		// グラフを描画する
		elemPriceDist.style = "display:inline-block;";

		if(typeof Flotr !== "undefined"){
			drawPrice();
		}else{
			loadScript(URLS.drawLib,drawPrice);
		}
	}
}



// グラフ:財務データ
function drawFinancialData(dataName){
	var yearElems = document.getElementsByClassName("financialYear");
	var dataElems = document.getElementsByClassName(dataName);
	var dataList = [];

	for(i=0; i<yearElems.length; i++){
		dataList[i] = [parseInt(yearElems[i].innerHTML), parseInt(dataElems[i].innerHTML)];
	}

	var drawIt = function(){
		var graphElem = document.getElementById("financialDataGraph");
		graphElem.style="display:block";
		Flotr.draw(
			document.getElementById("financialDataGraph"),
			[
				{ data: dataList, lines: {show:true} }
			],
			{
				title: document.getElementById(dataName+"Title").innerHTML,
				yaxis: {min:0}
			}
		);
		graphElem.scrollIntoView(true);
	}

	if(typeof Flotr !== "undefined"){
		drawIt();
	}else{
		loadScript(URLS.drawLib,drawIt);
	}
}

function drawFinancialDataAll(){
	var yearElems = document.getElementsByClassName("financialYear");
	var salesElems = document.getElementsByClassName("financialSales");
	var operatingProfitElems = document.getElementsByClassName("financialOperatingProfit");
	var netIncomeElems = document.getElementsByClassName("financialNetIncome");

	var salesDataList = [];
	var operatingProfitDataList = []
	var netIncomeDataList = []

	for(i=0; i<yearElems.length; i++){
		salesDataList[i] = [parseInt(yearElems[i].innerHTML), parseInt(salesElems[i].innerHTML)];
		operatingProfitDataList[i] = [parseInt(yearElems[i].innerHTML), parseInt(operatingProfitElems[i].innerHTML)];
		netIncomeDataList[i] = [parseInt(yearElems[i].innerHTML), parseInt(netIncomeElems[i].innerHTML)];
	}

	var drawIt = function(){
		var graphElem = document.getElementById("financialDataGraph");
		graphElem.style="display:block";
		Flotr.draw(
			document.getElementById("financialDataGraph"),
			[
				{ data: salesDataList, lines: {show:true}, label:"売上" },
				{ data: operatingProfitDataList, lines: {show:true}, yaxis:2, label:"営業利益"},
				{ data: netIncomeDataList, lines: {show:true}, yaxis:2, label:"純利益"},
			],
			{
				title: "",
				yaxis: {min:0},
				y2axis: {min:0}
			}
		);
		graphElem.scrollIntoView(true);
	}

	if(typeof Flotr !== "undefined"){
		drawIt();
	}else{
		loadScript(URLS.drawLib,drawIt);
	}
}



/*財務データ取得―検索*/
function searchFinancialData() {

	// 開いている財務データ表を閉じる
	var elemFinancialData = document.getElementById("fragFinancialData");
	if(elemFinancialData){
		elemFinancialData.outerHTML="";
	}

	// 証券コード取得してリクエスト
	var code = document.getElementById("codeInputForFinancialData").value;

	var params = {
		url:URLS.getFinancialData+code+"/"+currentDate,
		func200:displayFinancialDataToCode,
		mode:"text"
	}

	// 編集モード対応
	if(editMode){
		params.url += "/edit"
	} else{
		params.url += "/normal"		
	}

	makeRequest(params);

	// 表示コールバック
	function displayFinancialDataToCode(res){
		displayFinancialData(res, 0);
	}
}


/*財務データ取得―選択*/
function selectFinancialData(code){

	// 開いている財務データ表を閉じる場合など
	var elemFinancialData = document.getElementById("fragFinancialData");
	if(elemFinancialData){
		var codePre = document.getElementById("fragFinancialDataCode").innerHTML;
		elemFinancialData.outerHTML="";
		if(parseInt(code) === parseInt(codePre)){
			return code;
		}
	}

	// リクエスト
	var params = {
		url:URLS.getFinancialData+code+"/"+currentDate,
		func200:displayFinancialDataToCode,
		mode:"text"
	}

	if(editMode){
		params.url += "/edit"
	} else{
		params.url += "/normal"		
	}

	makeRequest(params);

	// 表示コールバック
	function displayFinancialDataToCode(res){
		displayFinancialData(res, code);
	}
}

/* 財務データ表示 */
function displayFinancialData(res, toId){
	document.getElementById(toId).innerHTML += res;
	if(toId){
		if(editMode){
			document.getElementById("fragFinancialDataAdd").style="display:none";
		}
		document.getElementById("fragFinancialDataStockInfo").style="display:none";		
	} else {
		if(editMode){
			document.getElementById("fragFinancialDataDelete").style="display:none";
		}
	}
}

/*
 * リクエスト関数
 */
function makeRequest(params) {
	// params.urlが必須
	// params.func200、params.method、params.funcNot200、params.funcErrorはオプション
	// params.mode [text]

	console.log("make Request:" + params.url);
	if (!params.method) {
		params.method = "GET";
	}
	var result;

	var xhr = new XMLHttpRequest();
	xhr.open(params.method, params.url, true);
	xhr.onload = function(e) {
		console.log("get response");
		if (xhr.readyState === 4) {
			if (xhr.status === 200) {
				console.log(xhr.responseText);

				//レスポンスがJSONではない場合、個別に”text”をparams.modeに渡す
				if(params.mode==="text"){
					result = xhr.response;
				} else{
					result = JSON.parse(xhr.response);
				}
				if (params.func200) {
					if (windowLoaded) {
						params.func200(result);
					} else {
						window.onload = function() {
							console.log("wait for window load: getMyStockList");
							params.func200(result);
						}
					}
				} else{
					return g_result=result;
				}
			} else {
				if (params.funcNot200) {
					params.funcNot200();
				} else {
					console.error(xhr.statusText);
				}
			}
		}
	};
	xhr.onerror = function(e) {
		if (params.funcError) {
			params.funcError();
		} else {
			console.error(xhr.statusText);
		}
	};
	xhr.send(null);
}
