package com.example.demo.entity;

import java.sql.Date;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
public class StockInfoEntity {
	private int code;
	private String name;
	private String description;
	private Date updated_at;

}
