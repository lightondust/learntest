package com.example.demo.entity;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
public class FinancialDataEntity {

	private String code;
	private String name;
	private String per;
	private String last;
	private String date;
	private String year;
	private float sales;
	private float operating_profit;
	private float net_income;

}
