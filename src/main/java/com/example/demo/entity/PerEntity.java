package com.example.demo.entity;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
public class PerEntity {
	   private Double per;
	   private Double last;

}
