package com.example.demo.controller;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.demo.entity.DateEntity;
import com.example.demo.entity.FinancialDataEntity;
import com.example.demo.entity.PerEntity;
import com.example.demo.entity.StockInfoEntity;
import com.example.demo.mapper.FinancialDataMapper;
import com.example.demo.mapper.PerMapper;
import com.example.demo.mapper.PriceMapper;
import com.example.demo.mapper.StockInfoMapper;

/*
 * デプロイ環境で/stock/下にあるため、
 * 開発環境では/stock/perでも動くように二つ目の/stockを付けている
 */
@RestController
@RequestMapping("/")
public class ContentController {
	private static final Logger logger = LoggerFactory.getLogger(ContentController.class);

	/*
	 * 直近の日付けを取得する
	 */
	@Autowired
	PriceMapper priceMapper;

	@RequestMapping(value = "/currentDate")
	public DateEntity currentDate() {
		logger.info("get current date");
		return priceMapper.currentDate();
	}
	
	@RequestMapping(value = "/dateList")
	public ArrayList<DateEntity> dateList() {
		logger.info("get current date");
		return priceMapper.dateList();
	}

	/*
	 * Per一覧取得
	 */
	@Autowired
	PerMapper perMapper;

	@RequestMapping(value = "/per/{date}")
	public ArrayList<PerEntity> perListByDate(@PathVariable String date) {
		logger.info("get per:" + date);
		return perMapper.perByDate(date);
	}
	
//	株価一覧取得
	@RequestMapping(value = "/price/{date}")
	public ArrayList<PerEntity> priceListByDate(@PathVariable String date) {
		logger.info("get price:" + date);
		return perMapper.priceByDate(date);
	}
	

	/*
	 * 財務データ取得、日付付きの場合はその日付の株価とPerも一緒に表示
	 */
	
	@Autowired
	FinancialDataMapper financialDataMapper;

	@RequestMapping(value = "/financialData/{code}")
	public ArrayList<FinancialDataEntity> financialData(@PathVariable int code) {
		logger.info("get financial data");
		return financialDataMapper.financialDataByCode(code);
	}

	@RequestMapping(value = "/financialData/{code}/{date}")
	public ArrayList<FinancialDataEntity> financialDataWithPer(@PathVariable int code, @PathVariable String date) {
		logger.info("get financial data with per");
		return financialDataMapper.financialDataByCodeWithPer(code, date);
	}

	/*
	 * 銘柄一覧を取得する
	 * バックアップ用
	 */
	@Autowired
	StockInfoMapper stockInfoMapper;

	@RequestMapping(value = "/stockList/{user}/{section}")
	public ArrayList<StockInfoEntity> stockList(@PathVariable String user, @PathVariable String section) {
		logger.info("get stockList");
		return stockInfoMapper.stockListByUser(user, section);
	}

}
