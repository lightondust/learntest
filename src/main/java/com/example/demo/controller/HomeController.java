package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.mapper.StockInfoMapper;

@Controller
@RequestMapping("/")
public class HomeController {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	StockInfoMapper stockInfoMapper;

	@RequestMapping(value = {"/top","/"})
	public String top(Model model) {
		logger.info("top.html");
		return "top";
	}

	@RequestMapping("/edit")
	public String edit(Model model) {
		logger.info("edit mode top.html");
		model.addAttribute("edit", true);
		return "top";
	}

	@RequestMapping("/debug")
	public String debug(Model model) {
		logger.info("debug mode top.html");
		model.addAttribute("debug", true);
		return "top";
	}
	
	@RequestMapping("/exp")
	public String experiment(Model model) {
		return "exp";
	}
	

}
