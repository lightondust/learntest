package com.example.demo.controller;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.entity.FinancialDataEntity;
import com.example.demo.mapper.FinancialDataMapper;
import com.example.demo.mapper.PerMapper;
import com.example.demo.mapper.StockInfoMapper;

@Controller
@RequestMapping("/fragment")
public class FragmentController {
	private static final Logger logger = LoggerFactory.getLogger(ContentController.class);

	// 財務データ取得
	@Autowired
	FinancialDataMapper financialDataMapper;

	@RequestMapping("/financialData/{code}/{date}/{mode}")
	String test(@PathVariable int code, @PathVariable String date, @PathVariable String mode, Model model) {
		logger.info("get financial data with per");
		ArrayList<FinancialDataEntity> financialData = financialDataMapper.financialDataByCodeWithPer(code, date);
		model.addAttribute("financialData", financialData);
		model.addAttribute("selectedDate", date);
		if(mode=="edit") {
			model.addAttribute("mode", "edit");
		} else {
			model.addAttribute("model","normal");
		}
		return "template :: financialData";
	}

	// perを昇順に取得する
	@Autowired
	PerMapper perMapper;

	@RequestMapping("/perSort/{date}")
	String perSort(@PathVariable String date, Model model) {
		logger.info("get financial data with per");
		model.addAttribute("perSort", perMapper.perSort(date));
		model.addAttribute("selectedDate", date);
		return "template :: perSort";
	}
	
	//銘柄一覧取得
	@Autowired
	StockInfoMapper stockInfoMapper;
	
	@RequestMapping("/stockList/{user}/{section}")
	String stockList(@PathVariable String user, @PathVariable String section, Model model) {
		logger.info("fragment stockList");
		model.addAttribute("stockList", stockInfoMapper.stockListByUser(user, section));
		model.addAttribute("user", user);
		model.addAttribute("section",section);
		return "template :: stockList";
	}

}