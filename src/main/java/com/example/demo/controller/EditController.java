package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.mapper.UserStockListMapper;

@RestController
@RequestMapping("/edit")
public class EditController {
	@Autowired
	UserStockListMapper userStockListMapper;

	@RequestMapping("/delete/{code}")
	public String delete(@PathVariable int code) {
		boolean res = userStockListMapper.deleteByCode(code);
		return "{\"delete\":"+res+"}";
	}

	@RequestMapping("/add/{code}")
	public String add(@PathVariable int code) {
		boolean res = userStockListMapper.addByCode(code);
		return "{\"add\":"+res+"}";
	}
}
