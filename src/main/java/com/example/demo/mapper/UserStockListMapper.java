package com.example.demo.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserStockListMapper {
	@Delete("delete from stock.user_stock_list where code=#{code} and user='default' and section = 'default'")
	boolean deleteByCode(int code);
	
	@Insert("Insert into stock.user_stock_list (code) values (#{code})")
	boolean addByCode(int code);

}
