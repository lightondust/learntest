package com.example.demo.mapper;


import java.util.ArrayList;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.example.demo.entity.DateEntity;


@Mapper
public interface PriceMapper {
	
    @Select("select date, count(code) as count from stock.price group by date having(count>3000) order by date desc limit 1;")
    DateEntity currentDate();
    
    @Select("select date, count(code) as count from stock.price group by date having(count>3000) order by date desc;")
    ArrayList<DateEntity> dateList();
}
