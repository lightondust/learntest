package com.example.demo.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.example.demo.entity.FinancialDataEntity;

@Mapper
public interface FinancialDataMapper {
	@Select("select stock_info.code, name, year, sales, operating_profit, net_income from stock.stock_info " + 
			"left outer join stock.financial_data " + 
			"on stock_info.code = financial_data.code " + 
			"where stock_info.code=#{code} order by year desc;")
	ArrayList<FinancialDataEntity> financialDataByCode(@Param("code") int code);

	@Select("select stock_info.code, name, per, last, date, year, sales, operating_profit, net_income from stock.stock_info " + 
			"left outer join stock.financial_data " + 
			"on stock_info.code = financial_data.code " + 
			"left outer join stock.price " + 
			"on stock_info.code = price.code " + 
			"where stock_info.code=#{code} and price.date=#{date} " + 
			"order by year desc;")
	ArrayList<FinancialDataEntity> financialDataByCodeWithPer(@Param("code") int code, @Param("date") String date);

}
