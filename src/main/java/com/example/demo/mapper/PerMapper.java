package com.example.demo.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.example.demo.entity.FinancialDataEntity;
import com.example.demo.entity.PerEntity;

@Mapper
public interface PerMapper {
    @Select("SELECT per FROM stock.price WHERE date = #{date} and per>0 order by per")
    ArrayList<PerEntity> perByDate(@Param("date") String date);
    
    @Select("select price.code, name, per " + 
    		"from stock.price " + 
    		"inner join stock.stock_info " + 
    		"on price.code = stock_info.code " + 
    		"where per>0 and date=#{date} order by per asc, code asc " + 
    		"limit 100;" )
    ArrayList<FinancialDataEntity> perSort(@Param("date") String date);    
    
    @Select("select price.code, name, per, year, sales, operating_profit, net_income " + 
    		"from stock.price " + 
    		"inner join stock.stock_info " + 
    		"on price.code = stock_info.code " + 
    		"left outer join stock.financial_data " + 
    		"on financial_data.code = price.code " + 
    		"where per>0 and date=#{date} order by per asc, code asc, year desc;")
    ArrayList<FinancialDataEntity> perSortDetail(@Param("date") String date);
    
    @Select("SELECT last FROM stock.price WHERE date = #{date} order by last;")
    ArrayList<PerEntity> priceByDate(@Param("date") String date);

}
