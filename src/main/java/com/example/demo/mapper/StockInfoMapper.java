package com.example.demo.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.example.demo.entity.StockInfoEntity;

@Mapper
public interface StockInfoMapper {
    @Select("select stock_info.code, name " + 
    		"from stock.stock_info " + 
    		"inner join stock.user_stock_list " + 
    		"on stock_info.code = user_stock_list.code " + 
    		"where user=#{user} and section=#{section} order by code asc")
    ArrayList<StockInfoEntity> stockListByUser(@Param("user") String user, @Param("section") String section);

}
